import click
import time
import json
import numpy as np
import pandas as pd

from matplotlib import pyplot as plt
from datetime import datetime, timedelta
from keras.layers import LSTM, RepeatVector, TimeDistributed, Input, Dense
from keras.models import Model, Sequential, model_from_json
from keras.constraints import max_norm
from copy import deepcopy
from calendar import timegm


class HourlyPredictor():
    def __init__(self, numhours=24):
        """ Initialize the prediction object.
        Numhours indicates the number of hours before target to use in prediction. Default is 24.
        """
        self.numhours = numhours
        self.mu = None

    def reshape_data(self, data):
        """ Function used to reshape hourly counts into labels (Y) which contain an hourly count, 
        and features (X), which contain the 24 (or whatever self.numhours is set to) previous hourly counts (X).
        """
        X = []
        Y = []
        for i in range(self.numhours, len(data)):
            X.append(data[i-self.numhours:i])
            Y.append(data[i])

        X = np.array(X)
        X = np.reshape(X, (X.shape[0], X.shape[1], 1))
        Y = np.reshape(np.array(Y), (len(Y), 1, 1))
        return(X, Y)

    def train(self, training_data, epochs=35, batchsize=256, neurons=50):
        """ Normalize input data, build the neural network, train the model.
        """
        self.normalized_train = self.normalize(training_data)
        self.X_train_norm, self.Y_train_norm = self.reshape_data(
            self.normalized_train)
        # Build model
        self.model = Sequential()
        self.model.add(LSTM(neurons, activation='linear', input_shape=(self.numhours, 1),
                            kernel_constraint=max_norm(3), recurrent_constraint=max_norm(3), bias_constraint=max_norm(3)))
        self.model.add(RepeatVector(1))
        self.model.add(LSTM(neurons, activation='linear', return_sequences=True,
                            kernel_constraint=max_norm(3), recurrent_constraint=max_norm(3), bias_constraint=max_norm(3)))
        self.model.add(TimeDistributed(Dense(1)))
        self.model.compile(optimizer='adam', loss='mse')
        # Train model
        self.history = self.model.fit(self.X_train_norm, self.Y_train_norm, epochs=epochs,
                                      batch_size=batchsize, verbose=0, validation_split=0.2)

    def test(self, test_data):
        """ Normalize input data, test the model, denormalize and return true values and predictions
        """
        self.normalized_test = self.normalize(test_data)
        self.X_test_norm, self.Y_test_norm = self.reshape_data(
            self.normalized_test)
        self.Y_pred_norm = self.model.predict(self.X_test_norm)
        self.Y_pred = self.denormalize(self.Y_pred_norm)
        self.Y_test = self.denormalize(self.Y_test_norm)
        return self.Y_test, self.Y_pred

    def train_from_dataframe(self, train_dataframe, epochs=35, batchsize=256, neurons=50):
        """ Train the model using a dataframe object as an input
        """
        cut_dataframe = self.cut_dataframe_at_mondays(train_dataframe)
        missing_labels = np.array(cut_dataframe['missing'], dtype='int')
        if all(missing_labels == 0):
            training_data = np.array(cut_dataframe['reportedcount1'])
            self.train(training_data, epochs, batchsize, neurons)
        else:
            print("ERROR: training dataframe cannot contain missing data. Shutting down.")
            exit()

    def test_from_dataframe(self, test_dataframe):
        """ Train the model using a dataframe object as an input
        """
        cut_dataframe = self.cut_dataframe_at_mondays(test_dataframe)
        test_data = np.array(cut_dataframe['reportedcount1'])
        test, pred = self.test(test_data)
        return test, pred

    def cut_times_at_mondays(self, times):
        """ Function used to get the index of the first 00:00 Monday in an array of times
        """
        T_datetime = [datetime.fromtimestamp(t) for t in times]
        is_monday = np.array([t.weekday() == 0 for t in T_datetime])
        is_00 = np.array([t.hour == 0 for t in T_datetime])
        start = np.where(is_00*is_monday == True)[0][0]
        return start

    def cut_dataframe_at_mondays(self, dataframe):
        """ Function used to cut at the first 00:00 Monday a dataframe object
        """
        times = dataframe['startdt']
        start_index = self.cut_times_at_mondays(times)
        cut_dataframe = dataframe[start_index:]
        return cut_dataframe

    def normalize(self, data):
        """ Used to perpare the data for the neural network.
        The average weekly profile (typical behaviour of the hourly counts during a week)
        is obtained from the data. 
        We then take the log of the raw data and the log of the repeated weekly profile, 
        get their difference, and then transform the result so that is has mean = 0 and 
        standard deviation = 1.
        """

        if self.mu is None:
            # Get the weekly profile and tile the dataset
            weeks_in_dataset = len(data)//168
            weekly_profile = np.reshape(
                data[:weeks_in_dataset*168], (len(data)//168, 168))
            weekly_profile = np.mean(weekly_profile, axis=0)
            self.weekly_profile_single = weekly_profile[:168]
            weekly_profile = np.tile(weekly_profile, len(data)//168 + 1)
            weekly_profile = weekly_profile[:len(data)]

            # Normalize by taking the logs of the counts and the weekly profile, and then subtracting them
            # +1 is here so that zeros do not break everything
            log_weekly_profile = np.log(weekly_profile+1)
            # +1 is here so that zeros do not break everything
            X_log = np.log(data+1)
            X_notrend_log = X_log - log_weekly_profile
            self.mu = np.mean(X_notrend_log)
            self.std = np.std(X_notrend_log, ddof=1)

        else:
            weekly_profile = np.tile(
                self.weekly_profile_single, len(data)//168 + 1)
            weekly_profile = weekly_profile[:len(data)]
            # +1 is here so that zeros do not break everything
            X_notrend_log = np.log(data + 1) - np.log(weekly_profile + 1)

        X_notrend_norm_log = (X_notrend_log - self.mu)/self.std
        return X_notrend_norm_log

    def denormalize(self, data):
        """ Used to reconstruct the original data from the output of the neural network.
        Does the inverse operations of normalize
        """
        weekly_profile_tile = np.tile(self.weekly_profile_single, len(
            data)//168 + 1)  # Tile for a week more
        # And then cut at the data length
        weekly_profile_tile = weekly_profile_tile[:len(data)]
        # This is needed here otherwise the following operations get messy with dimensions
        data = data[:, 0, 0]
        X_notrend_log = data*self.std + self.mu
        log_weekly_profile = np.log(weekly_profile_tile+1)
        X_log = X_notrend_log + log_weekly_profile
        X = np.exp(X_log) - 1
        return X

    def anomaly_detection(self, data_to_analyse, threshold_sensitivity = 6):
        """ Detect high and low anomalies on the test set.
        The anomaly detection first looks at 24-hour cumulative sum of the difference
        between model and observed data, labelling "intersting times" where the cumulative sum of the
        difference is far from zero.
        Secondly, the algorithm looks at he hourly count difference between model and observed data only within the "interesting times".
        Low anomalies use the threshold sensitivity, high anomalies use half of that sensitivity.
        The function saves to self the indexes of the anomalies detected.
        """
        self.data_to_analyse = data_to_analyse
        self.threshold_sensitivity = threshold_sensitivity
        self.normalized_data_ad = self.normalize(self.data_to_analyse)
        self.X_norm_ad, self.Y_norm_ad = self.reshape_data(self.normalized_data_ad)
        self.T_norm_ad = self.model.predict(self.X_norm_ad)

        # Get de-normlized hourly counts
        self.Y_ad = self.denormalize(self.Y_norm_ad)
        self.T_ad = self.denormalize(self.T_norm_ad)

        # Calulcate difference and daily cumulative difference between model and observations
        diff = self.Y_ad - self.T_ad
        cum_diff = np.zeros(len(diff) - self.numhours)
        for i in np.arange(self.numhours):
            cum_diff += diff[i:-self.numhours+i]

        daily_treshold = np.mean(self.Y_ad)*24/self.threshold_sensitivity
        self.interesting_times = 12+np.where(abs(cum_diff) > daily_treshold)[0]

        low_threshold = np.mean(self.Y_ad)/(self.threshold_sensitivity/np.sqrt(24))
        self.low_anomaly = np.where(abs(diff[self.interesting_times]) > low_threshold)[0]
        self.high_anomaly = np.where(abs(diff[self.interesting_times]) > low_threshold*2)[0]

        return self.low_anomaly, self.high_anomaly

    def anomaly_detection_from_dataframe(self, dataframe, threshold_sensitivity = 6):
        """ Analyse anomalies on a dataframe object
        """
        cut_dataframe = self.cut_dataframe_at_mondays(dataframe)
        data_to_analyse = np.array(cut_dataframe['reportedcount1'])
        low_anomaly, high_anomaly = self.anomaly_detection(data_to_analyse, threshold_sensitivity)
        modified_cut_df = self.write_anomalies(cut_dataframe)
        return modified_cut_df

    def write_anomalies(self, df):
        """ Add the anomaly column to a dataframe
        """
        # Create an array for the parts of the dataframe that have been taken into account by the algorithm
        anomaly_index = np.zeros(len(df))
        anomaly_index[self.low_anomaly] = 1 # Low anomaly
        anomaly_index[self.high_anomaly] = 2 # High anomaly
        # Put the anomaly values into the dataframe
        df['anomaly'] = anomaly_index
        return df

    def save(self, location="../models/"):
        """ Save model and its normalization parameters to json,
        and model's weights to h5 files at location
        """
        # serialize model to JSON
        model_json = self.model.to_json()
        with open(location + "hourly_anomaly_detection_model.json", "w") as json_file:
            json_file.write(model_json)
        # serialize weights to HDF5
        self.model.save_weights(
            location + "hourly_anomaly_detection_weights.h5")

        parameters = {'mu': self.mu, 'std': self.std,
                      'weekly_profile': list(self.weekly_profile_single)}
        with open(location + "hourly_anomaly_detection_parameters.json", "w") as fp:
            json.dump(parameters, fp, indent=4)

        print("Saved model to disk")

    def load(self, location="../models/"):
        """ Load model and its normalization parameters from json,
        and model's weights from h5 files at location
        """
        # load json and create model
        json_file = open(location + 'hourly_anomaly_detection_model.json', 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        loaded_model = model_from_json(loaded_model_json)
        # load weights into new model
        loaded_model.load_weights(
            location + "hourly_anomaly_detection_parameters.h5")
        self.model = loaded_model

        with open(location + 'hourly_imputation_normalization_parameters.json') as json_file:
            parameters = json.load(json_file)
        self.mu, self.std = parameters['mu'], parameters['std']
        self.weekly_profile_single = np.array(parameters['weekly_profile'])
        print("Loaded model from disk")

    def plot_anomaly(self, fig_name=None):
        """ Plot the observed counts, the model prediction of the counts, highlighting
        low and high anomalies in the test set.
        """
        plt.figure(figsize=(20, 10))
        plt.plot(self.Y_ad, label = 'Real counts')
        plt.plot(self.T_ad, label = 'Model counts')
        plt.plot(np.arange(len(self.Y_ad))[self.interesting_times][self.low_anomaly], 
            np.zeros(len(self.Y_ad))[self.interesting_times][self.low_anomaly], 'yo', label = 'Low Level Anomaly')
        plt.plot(np.arange(len(self.Y_ad))[self.interesting_times][self.high_anomaly], 
            np.zeros(len(self.Y_ad))[self.interesting_times][self.high_anomaly], 'ro', label = 'High Level Anomaly')
        plt.xlabel('Hour')
        plt.xlabel('Hourly Counts')
        plt.legend()
        if fig_name is not None:
            plt.savefig(fig_name + ".png", dpi=80)
            plt.close()
        else:
            plt.show()


def get_dataframe(folder='../data/aggregatedCounts2016.csv', locid=2):
    """ Obtain and prepare real data from a legacy database.
    The format of the data is such that it should be almost identical to the one used in the pipeline.
    """
    real_data = pd.read_csv('../data/aggregatedCounts2016.csv', sep=',',
                            names=['areaid', 'devid', 'time', 'reportedcount1'], skiprows=[0])
    df = real_data.loc[np.where(real_data['areaid'] == locid)[0]]

    start_epochs = np.array(
        [timegm(time.strptime(t, "%Y-%m-%d %H:%M:%S.%f")) for t in df['time']])
    end_epochs = start_epochs + 3600 - 60
    df['reportedcount2'] = np.zeros(len(df['reportedcount1']), dtype=int)
    df['startdt'] = start_epochs
    df['enddt'] = end_epochs
    df['level'] = "1h"
    df['missing'] = 0
    df['quality'] = 1
    df = df.drop('time', 1)
    df = df.drop('devid', 1)
    return df


def detect(df, training_weeks = 7, test_weeks = 10, threshold = 6.0):
    a = HourlyPredictor(24)
    a.train_from_dataframe(df[:int(168*(training_weeks+1))])
    df_anomalies = a.anomaly_detection_from_dataframe(df[int(168*(training_weeks+1)):int(168*(training_weeks+test_weeks+1))], threshold)
    a.plot_anomaly(fig_name = "../plots/hourly_anomaly_detection_plot")



@click.command()
# Location ID inside the dataframe
@click.option("--location", '-l', type=int, default=2)
# How many weeks to use in training, these are always the first weeks of the ones that are used
@click.option("--training_weeks", "-ntr", type=int, default=7)
# How many weeks to use in testing, these follow immediately the training weeks
@click.option("--test_weeks", "-ntst", type=int, default=10)
# Location where to save the model to, and load the model from
@click.option("--model_location", "-ml", type=str, default=None)
# Value of the anomaly detection theshold
@click.option("--threshold", "-t", type=float, default=6.0)
def main(location, training_weeks, test_weeks, model_location, threshold):

    df = get_dataframe(
        folder='../data/aggregatedCounts2016.csv', locid=location)
    detect(df, threshold, training_weeks, test_weeks)


if __name__ == "__main__":
    main()