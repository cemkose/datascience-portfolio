import click
import time
import json
import numpy as np
import pandas as pd

from matplotlib import pyplot as plt
from datetime import datetime, timedelta
from keras.layers import LSTM, RepeatVector, TimeDistributed, Input, Dense
from keras.models import Model, Sequential, model_from_json
from keras.constraints import max_norm
from copy import deepcopy
from calendar import timegm


class DailyPredictor():
    def __init__(self, numdays=14):
        """ Initialize the prediction object.
        numdays indicates the number of days before target to use in prediction. Default is 14.
        """
        self.numdays = numdays
        self.mu = None

    def reshape_data(self, data):
        """ Function used to reshape hourly counts into labels (Y) which contain an hourly count, 
        and features (X), which contain the 24 (or whatever self.numdays is set to) previous hourly counts (X).
        """
        X = []
        Y = []
        for i in range(self.numdays, len(data)):
            X.append(data[i-self.numdays:i])
            Y.append(data[i])

        X = np.array(X)
        X = np.reshape(X, (X.shape[0], X.shape[1], 1))
        Y = np.reshape(np.array(Y), (len(Y), 1, 1))
        return(X, Y)

    def train(self, training_data, epochs=100, batchsize=128, neurons=50):
        """ Normalize input data, build the neural network, train the model.
        """
        self.normalized_train = self.normalize(training_data)
        self.X_train_norm, self.Y_train_norm = self.reshape_data(
            self.normalized_train)
        # Build model
        self.model = Sequential()
        self.model.add(LSTM(neurons, activation='linear', input_shape=(self.numdays, 1),
                            kernel_constraint=max_norm(3), recurrent_constraint=max_norm(3), bias_constraint=max_norm(3)))
        self.model.add(RepeatVector(1))
        self.model.add(LSTM(neurons, activation='linear', return_sequences=True,
                            kernel_constraint=max_norm(3), recurrent_constraint=max_norm(3), bias_constraint=max_norm(3)))
        self.model.add(TimeDistributed(Dense(1)))
        self.model.compile(optimizer='adam', loss='mse')
        # Train model
        self.history = self.model.fit(self.X_train_norm, self.Y_train_norm, epochs=epochs,
                                      batch_size=batchsize, verbose=0, validation_split=0.2)

    def test(self, test_data):
        """ Normalize input data, test the model, denormalize and return true values and predictions
        """
        self.normalized_test = self.normalize(test_data)
        self.X_test_norm, self.Y_test_norm = self.reshape_data(
            self.normalized_test)
        self.Y_pred_norm = self.model.predict(self.X_test_norm)
        self.Y_pred = self.denormalize(self.Y_pred_norm)
        self.Y_test = self.denormalize(self.Y_test_norm)
        return self.Y_test, self.Y_pred

    def train_from_dataframe(self, train_dataframe, epochs=100, batchsize=128, neurons=50):
        """ Train the model using a dataframe object as an input
        """
        cut_dataframe = self.cut_dataframe_at_mondays(train_dataframe)
        missing_labels = np.array(cut_dataframe['missing'], dtype='int')
        if all(missing_labels == 0):
            training_data = np.array(cut_dataframe['reportedcount1'])
            self.train(training_data, epochs, batchsize, neurons)
        else:
            print("ERROR: training dataframe cannot contain missing data. Shutting down.")
            exit()

    def test_from_dataframe(self, test_dataframe):
        """ Train the model using a dataframe object as an input
        """
        cut_dataframe = self.cut_dataframe_at_mondays(test_dataframe)
        test_data = np.array(cut_dataframe['reportedcount1'])
        test, pred = self.test(test_data)
        return test, pred

    def cut_times_at_mondays(self, times):
        """ Function used to get the index of the first Monday in an array of times
        """
        T_datetime = [datetime.fromtimestamp(t) for t in times]
        is_monday = np.array([t.weekday() == 0 for t in T_datetime])
        start = np.where(is_monday == True)[0][0]

        return start

    def cut_dataframe_at_mondays(self, dataframe):
        """ Function used to cut at the first  Monday a dataframe object
        """
        times = dataframe['startdt']
        start_index = self.cut_times_at_mondays(times)
        cut_dataframe = dataframe[start_index:]
        return cut_dataframe

    def normalize(self, data):
        """ Used to perpare the data for the neural network.
        The average weekly profile (typical behaviour of the hourly counts during a week)
        is obtained from the data. 
        We then take the log of the raw data and the log of the repeated weekly profile, 
        get their difference, and then transform the result so that is has mean = 0 and 
        standard deviation = 1.
        """

        if self.mu is None:
            # Get the weekly profile and tile the dataset
            weeks_in_dataset = len(data)//7
            weekly_profile = np.reshape(
                data[:weeks_in_dataset*7], (len(data)//7, 7))
            weekly_profile = np.mean(weekly_profile, axis=0)
            self.weekly_profile_single = weekly_profile[:7]
            weekly_profile = np.tile(weekly_profile, len(data)//7 + 1)
            weekly_profile = weekly_profile[:len(data)]

            # Normalize by taking the logs of the counts and the weekly profile, and then subtracting them
            X_notrend = data - weekly_profile
            self.mu = np.mean(X_notrend)
            self.std = np.std(X_notrend, ddof=1)

        else:
            weekly_profile = np.tile(
                self.weekly_profile_single, len(data)//7 + 1)
            weekly_profile = weekly_profile[:len(data)]
            X_notrend = data - weekly_profile

        X_notrend_norm = (X_notrend - self.mu)/self.std
        return X_notrend_norm

    def denormalize(self, data):
        """ Used to reconstruct the original data from the output of the neural network.
        Does the inverse operations of normalize
        """
        weekly_profile_tile = np.tile(self.weekly_profile_single, len(
            data)//7 + 1)  # Tile for a week more
        # And then cut at the data length
        weekly_profile_tile = weekly_profile_tile[:len(data)]
        # This is needed here otherwise the following operations get messy with dimensions
        data = data[:, 0, 0]
        X_notrend = data*self.std + self.mu
        X = X_notrend + weekly_profile_tile
        return X

    def impute(self, data_to_impute, missing_labels):
        """ Look for data where the missing label is present an impute them based on the 24 hours of previous data.
        While imputing, save the imputed values and replace missing data.
        """
        self.impute_data = data_to_impute
        self.missing_labels = missing_labels
        self.normalized_impute_data = self.normalize(self.impute_data)

        self.X_imputed_norm, _ = self.reshape_data(self.normalized_impute_data)
        self.Y_imputed_norm = np.reshape(
            self.normalized_impute_data, (len(self.normalized_impute_data), 1, 1))
        for i in np.where(self.missing_labels == 1)[0]:
            try:
                pred = self.model.predict(np.reshape(
                    self.X_imputed_norm[i-self.numdays, :, 0], (1, self.numdays, 1)))
                self.Y_imputed_norm[i] = pred
                for t in range(self.numdays):
                    if(i+1-self.numdays+t < len(self.X_imputed_norm)):
                        self.X_imputed_norm[i+1-self.numdays +
                                            t, self.numdays-1-t, 0] = pred

            except:
                print("WARNING: Cannot impute data that has no counts for the previous %i hours." % (
                    self.numdays))
                pass

        self.Y_imputed = self.denormalize(self.Y_imputed_norm)
        return self.impute_data, self.Y_imputed

    def impute_from_dataframe(self, dataframe):
        """ Impute from a dataframe object which has the "missing" label
        """
        cut_dataframe = self.cut_dataframe_at_mondays(dataframe)
        missing_labels = np.array(cut_dataframe['missing'], dtype='int')
        data_to_impute = np.array(cut_dataframe['reportedcount1'])
        test, pred = self.impute(data_to_impute, missing_labels)
        return test, pred

    def save(self, location="../models/"):
        """ Save model and its normalization parameters to json,
        and model's weights to h5 files at location
        """
        # serialize model to JSON
        model_json = self.model.to_json()
        with open(location + "daily_imputation_model.json", "w") as json_file:
            json_file.write(model_json)
        # serialize weights to HDF5
        self.model.save_weights(location + "daily_imputation_model_weights.h5")

        parameters = {'mu': self.mu, 'std': self.std,
                      'weekly_profile': list(self.weekly_profile_single)}
        with open(location + "daily_imputation_normalization_parameters.json", "w") as fp:
            json.dump(parameters, fp, indent=4)

        print("Saved model to disk")

    def load(self, location="../models/"):
        """ Load model and its normalization parameters from json,
        and model's weights from h5 files at location
        """
        # load json and create model
        json_file = open(location + 'daily_imputation_model.json', 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        loaded_model = model_from_json(loaded_model_json)
        # load weights into new model
        loaded_model.load_weights(
            location + "daily_imputation_model_weights.h5")
        self.model = loaded_model

        with open(location + 'daily_imputation_normalization_parameters.json') as json_file:
            parameters = json.load(json_file)
        self.mu, self.std = parameters['mu'], parameters['std']
        self.weekly_profile_single = np.array(parameters['weekly_profile'])
        print("Loaded model from disk")

    def plot_imputation(self, fig_name=None):
        plt.figure(figsize=(20, 10))
        plt.plot(self.impute_data, label='Observed counts')
        plt.plot(self.Y_imputed, label='Imputed counts')

        plt.plot(np.arange(len(self.Y_imputed))[np.where(self.missing_labels == 1)[0]],
                 np.zeros(len(self.Y_imputed))[np.where(self.missing_labels == 1)[0]], 'r.', label='Missing data')
        plt.xlabel('Hour')
        plt.xlabel('Hourly Counts')
        plt.legend()
        if fig_name is not None:
            plt.savefig(fig_name + ".png", dpi=80)
            plt.close()
        else:
            plt.show()


def get_dataframe(folder='../data/aggregatedCounts2016.csv', locid=2):
    """ Obtain and prepare real data from a legacy database.
    The format of the data is such that it should be almost identical to the one used in the pipeline.
    """
    real_data = pd.read_csv(folder, sep=',', names=[
                            'areaid', 'devid', 'time', 'reportedcount1'], skiprows=[0])
    df = real_data.loc[np.where(real_data['areaid'] == locid)[0]]

    start_epochs = np.array(
        [timegm(time.strptime(t, "%Y-%m-%d %H:%M:%S.%f")) for t in df['time']])
    end_epochs = start_epochs + 3600 - 60
    df['reportedcount2'] = np.zeros(len(df['reportedcount1']), dtype=int)
    df['startdt'] = start_epochs
    df['enddt'] = end_epochs
    df['level'] = "1h"
    df['missing'] = 0
    df['quality'] = 1
    df = df.drop('time', 1)
    df = df.drop('devid', 1)
    return df


def get_hourly_dataframe(df):
    """ Function used in testing to generate a database od daily counts
    """

    b = DailyPredictor(14)
    cut_df = b.cut_dataframe_at_mondays(df)
    counts = np.array(cut_df['reportedcount1'])
    total_days = len(counts)//24
    daily_data = counts[:total_days*24]
    daily_data = np.reshape(daily_data, (total_days, 24))
    daily_data = np.sum(daily_data, axis=1)
    daily_df = pd.DataFrame()
    daily_df['reportedcount1'] = daily_data
    daily_df['missing'] = 0
    daily_df['startdt'] = 1437732000 + 3600*24*np.arange(len(daily_df))

    return daily_df


def test_accuracy(dataframe, train_weeks=52, test_weeks=20, fig_name=None):
    """ Function used to predict imputation accuracy on gaps of days, and weeks.
        The model is trained and then tested on never-seen data which contains missing values
        which the model has to guess. Error is calculated as the % difference between the guess
        and the actual values of the observation at that time.
    """
    a = DailyPredictor(14)
    df_train = dataframe[:7*train_weeks]
    df_test = dataframe[7*train_weeks:7*(train_weeks+test_weeks)]
    a.train_from_dataframe(df_train)

    # Test on single days misisng, chosen at random
    missing_days = np.zeros(len(df_test))
    index_days = np.random.choice(
        np.arange(1, len(df_test)), size=test_weeks, replace=False)
    for i in index_days:
        missing_days[i] = 1
    df_test_missingdays = deepcopy(df_test)
    df_test_missingdays['missing'] = missing_days
    y, t = a.impute_from_dataframe(df_test_missingdays)

    mean_daily_perc_err_days = np.mean(np.abs(
        100*(1+t[np.where(a.missing_labels == 1)[0]])/(1+y[np.where(a.missing_labels == 1)[0]]) - 100))

    print("Imputing daily gaps:")
    print("Mean daily  %% error on inputed data: %.2f %%" %
          (mean_daily_perc_err_days))
    if fig_name is not None:
        a.plot_imputation(fig_name + "daily")
    else:
        a.plot_imputation()

    # Test on single days misisng, chosen at random
    missing_week = np.zeros(len(df_test))
    index_week = np.random.choice(
        np.arange(1, test_weeks), size=test_weeks//7 + 1, replace=False)
    for i in index_week:
        missing_week[7*i:7*i+7] = 1
    df_test_missingweek = deepcopy(df_test)
    df_test_missingweek['missing'] = missing_week
    y, t = a.impute_from_dataframe(df_test_missingweek)

    mean_daily_perc_err_week = np.mean(np.abs(
        100*(1+t[np.where(a.missing_labels == 1)[0]])/(1+y[np.where(a.missing_labels == 1)[0]]) - 100))

    mean_weekly_perc_err_week = 0
    for i in index_week:
        mean_weekly_perc_err_week += abs(100*(1+np.sum(t[7*i:7*i+7]))/(
            1+np.sum(y[7*i:7*i+7])) - 100) / len(index_week)

    print("Imputing weekly gaps:")
    print("Mean daily  %% error on inputed data: %.2f %%" %
          (mean_daily_perc_err_week))
    print("Mean weekly %% error on inputed data: %.2f %%" %
          (mean_weekly_perc_err_week))
    if fig_name is not None:
        a.plot_imputation(fig_name + "weekly")
    else:
        a.plot_imputation()


@click.command()
# Location ID inside the dataframe
@click.option("--location", '-l', type=int, default=2)
# How many weeks to use in training, these are always the first weeks of the ones that are used
@click.option("--training_weeks", "-ntr", type=int, default=52)
# How many weeks to use in testing, these follow immediately the training weeks
@click.option("--test_weeks", "-ntst", type=int, default=20)
# Location where to save the model to, and load the model from
@click.option("--model_location", "-ml", type=str, default=None)
def main(location, training_weeks, test_weeks, model_location):

    df = get_dataframe(
        folder='../data/aggregatedCounts2016.csv', locid=location)
    daily_df = get_hourly_dataframe(df)
    test_accuracy(daily_df, training_weeks, test_weeks,
                  fig_name="../plots/daily_imputation_error_plot")


if __name__ == "__main__":
    main()