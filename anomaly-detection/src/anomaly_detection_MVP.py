import numpy as np
from matplotlib import pyplot as plt
from datetime import datetime, timedelta
from pandas.plotting import register_matplotlib_converters
import click

register_matplotlib_converters()

def generate_fake_data(weeks, noise = 0.5, vol = 2000, trend_intensity = 0.0):
    """ Duration in days, data is generated with an hourly granularity
        Volume is in people per hour, noise varies between 0 and 1
    """
    days = weeks*7
    hours = days *24
    t = np.linspace(0, hours - 1,hours)  # Time in hours
    # Daily trend
    d = np.sin(np.pi*(t-3)/24)**4
    # Noisy daily trend
    d_n = (d*(1+noise*np.random.rand(hours)))
    # Weekly trend, where the weekend has more people (the -54 shift)
    w = 0.875 + 0.25*np.sin(np.pi*(t-54)/24/7)**2
    # The weekly data should contain a noise which has some memory
    var = np.random.rand(weeks*7*24)
    weekly_noise = np.ones(var.size)
    for i in np.arange(var.size):
        weekly_noise[i] = 2.0*np.sum(var[:i] * np.exp(-(1+i-np.arange(i))/24))/np.sum(np.exp(-(1+i-np.arange(i))/24))
    weekly_noise[0:24] = 1.0
    # Noisy weekly data, average should still be 1
    w_n = (w*(1+weekly_noise))
    # Final data
    random_slope = trend_intensity*np.random.rand()
    slope_correction = np.linspace(1- random_slope, 1+random_slope , hours)
    timeseries = np.array(abs(vol*w_n*d_n *slope_correction), dtype = 'int')   # 1.708 makes it so that the average hourly count is vol
    return(timeseries)

class AnomalyDetection:
    def __init__(self, hourly_data):
        self.t_h = hourly_data['time']
        self.c_h = hourly_data['counts']
        self.a_h = np.zeros(len(hourly_data['time']), dtype = 'bool')
        self.extract_weekly_data()

    def extract_weekly_data(self):
        """ Function that converts the hourly count to a weekly count. 
            Weeks always start on 00:00 on monday and end on 23:59 on Sunday.
            Only data within full weeks are counted.
            Returns a dictionary containing weekly counts.
        """
        times_h = self.t_h
        counts_h = self.c_h
        try:
            times_h_date = np.array([datetime.strptime(i, "%Y-%m-%d %H:%M") for i in times_h])
        except:
            times_h_date = np.array([datetime.strptime(i, "%Y-%m-%d %H:%M:%S.%f") for i in times_h])
        startday = times_h_date[0].weekday()
        starthour = times_h_date[0].hour
        endday = times_h_date[-1].weekday()
        endhour = times_h_date[-1].hour

        # Get the date of the first midnight on monday before the start of data
        w_start_date = times_h_date[0] - timedelta(days = (startday)%7) - timedelta(hours = starthour)
        # Get the date of the last midnight of a monday after the end of data
        w_end_stamp = times_h_date[-1] - timedelta(days = endday) - timedelta(hours = endhour) + timedelta(days = 7)
        # Get tbe number of full weeks for which there is some data
        num_weeks = int((w_end_stamp - w_start_date).days/7)
        # Get the dates of every week's start
        start_of_week_date = np.linspace(w_start_date.timestamp(), (w_end_stamp - timedelta(days = 7)).timestamp(), num_weeks)
        # Get the dates for the weeks starts
        times_w = [datetime.strftime(datetime.fromtimestamp(i), "%Y-%m-%d %H:%M") for i in start_of_week_date]
        times_w_date = [datetime.fromtimestamp(i) for i in start_of_week_date]
        counts_w = np.zeros(len(times_w))
        
        # Indexes indicating to which week every hourly count belongs to 
        week_index = np.zeros(len(times_h_date))
        # Indexes indicating to which hour of the week every hourly count belongs to 
        hour_index = np.zeros(len(times_h_date))
        # Mean hourly count for the week
        self.avg_c_w = np.zeros(len(counts_w))
        for t_w, k in zip(times_w_date, range(len(times_w_date))):
            data_in_this_week = np.where((times_h_date >= t_w) 
                                                 & (times_h_date < t_w +timedelta(days = 7)))[0]
            # Get an index that keeps track of which week each hourly count belongs to
            week_index[data_in_this_week] = k
            # Get an index that keeps track of which hour of the week each hourly count belongs to
            hour_index[data_in_this_week] = [(i - t_w).seconds//3600 + (i - t_w).days*24 for i in times_h_date[data_in_this_week]]
            counts_ = counts_h[data_in_this_week]
            counts_w[k] = sum(counts_)
            try:
                self.avg_c_w[k] = float(sum(counts_)/len(counts_h[data_in_this_week]))
            except:
                self.avg_c_w[k] = 0.
        
        self.t_w = times_w
        self.c_w = counts_w
        self.week_index = week_index
        self.hour_index = hour_index
        self.a_w = np.zeros(len(counts_w), dtype = 'bool')

    def w_prop_anomaly_detection(self, prop):
        """ Anomaly detection based on the previous datapoint.
            Returns a boolean array which is True where a datapoint is more than 
            prop times bigger or smaller than the previous point.
        """
        anomalies = 1 + np.where(abs(self.avg_c_w[1:]/self.avg_c_w[:-1] - 1 ) > prop)[0]
        self.a_w[anomalies] = True
        return anomalies
    
    def h_max_anomaly_detection(self, numstd):
        """ Anomaly detection for a series, returns a list of the indexes
            where the series exceeds numstd median absolute deviations from the median
        """
        t = np.arange(len(self.t_h))
        p, m = np.polyfit(t, self.c_h, 1)
        series = self.c_h - p * t # Detrend in order to avoid detecting increasing peaks as anomlaies
        mean = np.median(series)
        std = np.std(series, ddof = 1)
        anomalies = np.where((series - mean) > numstd*std)[0]
        self.a_h[anomalies] = True
        return anomalies

    def h_tsd_anomaly_detection(self, threshold):
        """ Anomaly detection for a hourly series, returns a list of the 
            indexes where the residuals are more than threshold times larger than
            the series' median.
        """    
        modeled_hourly_counts = np.zeros(len(self.c_h))
        normalized_h_count = np.zeros(len(self.c_h))
        weekly_mean = np.zeros(len(self.c_w))
        for i, count in zip(range(len(self.c_w)), self.c_w):
            this_week_index = np.where(self.week_index == i)[0]
            normalized_h_count[this_week_index] = self.c_h[this_week_index] / self.avg_c_w[i]
            modeled_hourly_counts[this_week_index] = self.avg_c_w[i]
            
        normalized_hourly_mean = np.zeros(24*7)
        for i in range(24*7):
            this_hour_index = np.where(self.hour_index == i)[0]
            normalized_hourly_mean[i] = np.mean(normalized_h_count[this_hour_index])
            modeled_hourly_counts[this_hour_index] *= normalized_hourly_mean[i]

        residuals = self.c_h - modeled_hourly_counts
        anomalies = np.where(abs((residuals- np.std(residuals, ddof =1)/2)
                                 /np.std(residuals, ddof =1)) > threshold)[0]
        self.a_h[anomalies] = True
        return anomalies

    def plot(self):
        """ Plot the hourly count and weekly count, and highlight anomalies.
        """
        try:
            times_h = np.array([datetime.strptime(i, "%Y-%m-%d %H:%M") for i in self.t_h])
        except:
            times_h = np.array([datetime.strptime(i, "%Y-%m-%d %H:%M:%S.%f") for i in self.t_h])

        plt.plot(times_h, self.c_h, label = 'Hourly')
        h_anom = np.where(self.a_h ==  True)[0]
        plt.plot(times_h[h_anom], self.c_h[h_anom], 'ro', label= 'Hourly anomalies')
        try:
            times_w = np.array([datetime.strptime(i, "%Y-%m-%d %H:%M") for i in self.t_w])
        except:
            times_w = np.array([datetime.strptime(i, "%Y-%m-%d %H:%M:%S.%f") for i in self.t_w])
        plt.plot(times_w + timedelta(days = 3.5), self.avg_c_w, 'go-', label = 'Weekly average')
        w_anom = np.where(self.a_w ==  True)[0]
        plt.plot(times_w[w_anom] + timedelta(days = 3.5), self.avg_c_w[w_anom], 'ko', label= 'Weekly anomalies')
        plt.xlabel('Date')
        plt.ylabel('Hourly count')
        plt.legend(bbox_to_anchor=(1.1, 1.05))
        plt.show()
        return
    
    def get_all_anomalies(self, w_prop = 0.3, h_max_thresh = 5, h_tsd_thresh = 3):
        _ = self.h_tsd_anomaly_detection(h_tsd_thresh)
        _ = self.h_max_anomaly_detection(h_max_thresh)
        _ = self.w_prop_anomaly_detection(w_prop)


@click.command()
@click.option("--weeks", '-w', type=int, default=30)
@click.option("--noise", '-n', type=float, default=0.5)
@click.option("--trend", "-t", type=float, default=0.1)
@click.option("--count_mean", "-cm", type=int, default=2000)
@click.option("--hour_stds_thre", "-hs", type=float, default=5)
@click.option("--hour_tsd_thre", "-ht", type=float, default=5.0)
@click.option("--week_var", "-wv", type=float, default=0.25)

def main(weeks, noise, trend, count_mean, hour_stds_thre, hour_tsd_thre, week_var):
    # Generate fake hourly counts
    data ={}
    data['counts'] = generate_fake_data(weeks, noise, count_mean, trend)
    starttime = "2019-02-02 08:00"
    startstamp = datetime.strptime(starttime, "%Y-%m-%d %H:%M")
    endstamp = startstamp + timedelta(weeks = weeks)
    countstamps = np.linspace(startstamp.timestamp(), endstamp.timestamp() - 3600, weeks*24*7)
    data['time'] = [datetime.strftime(datetime.fromtimestamp(i), "%Y-%m-%d %H:%M")  for i in countstamps]
    
    # Plot counts with highlighted anomalies
    a = AnomalyDetection(data)
    a.get_all_anomalies(week_var, hour_stds_thre, hour_tsd_thre)
    a.plot()
        
if __name__ == "__main__":
    main()