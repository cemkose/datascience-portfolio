import click
import time
import json
import numpy as np
import pandas as pd

from matplotlib import pyplot as plt
from datetime import datetime, timedelta
from keras.layers import LSTM, RepeatVector, TimeDistributed, Input, Dense
from keras.models import Model, Sequential, model_from_json
from keras.constraints import max_norm
from copy import deepcopy
from calendar import timegm


class MinutePredictor():
    def __init__(self, num5mins=36):
        """ Initialize the prediction object.
        num5mins indicates the number of 5 minute segments before target to use in prediction. Default is 24.
        """
        self.num5mins = num5mins
        self.mu = None

    def reshape_data(self, data):
        """ Function used to reshape 5 minute counts into labels (Y) which contain a 5 minute count, 
        and features (X), which contain the 24 (or whatever self.num5mins is set to) previous 5 minute counts (X).
        """
        X = []
        Y = []
        for i in range(self.num5mins, len(data)):
            X.append(data[i-self.num5mins:i])
            Y.append(data[i])

        X = np.array(X)
        X = np.reshape(X, (X.shape[0], X.shape[1], 1))
        Y = np.reshape(np.array(Y), (len(Y), 1, 1))
        return(X, Y)

    def train(self, training_data, epochs=10, batchsize=512, neurons=100):
        """ Normalize input data, build the neural network, train the model.
        """
        self.normalized_train = self.normalize(training_data)
        self.X_train_norm, self.Y_train_norm = self.reshape_data(
            self.normalized_train)
        # Build model
        self.model = Sequential()
        self.model.add(LSTM(neurons, activation='linear', input_shape=(self.num5mins, 1),
                            kernel_constraint=max_norm(2), recurrent_constraint=max_norm(2), bias_constraint=max_norm(2)))
        self.model.add(RepeatVector(1))
        self.model.add(LSTM(neurons, activation='linear', return_sequences=True,
                            kernel_constraint=max_norm(2), recurrent_constraint=max_norm(2), bias_constraint=max_norm(2)))
        self.model.add(TimeDistributed(Dense(1)))
        self.model.compile(optimizer='adam', loss='mse')
        # Train model
        self.history = self.model.fit(self.X_train_norm, self.Y_train_norm,
                                      epochs=epochs, batch_size=batchsize, verbose=0, validation_split=0.2)

    def test(self, test_data):
        """ Normalize input data, test the model, denormalize and return true values and predictions
        """
        self.normalized_test = self.normalize(test_data)
        self.X_test_norm, self.Y_test_norm = self.reshape_data(
            self.normalized_test)
        self.Y_pred_norm = self.model.predict(self.X_test_norm)
        self.Y_pred = self.denormalize(self.Y_pred_norm)
        self.Y_test = self.denormalize(self.Y_test_norm)
        return self.Y_test, self.Y_pred

    def train_from_dataframe(self, train_dataframe, epochs=10, batchsize=512, neurons=100):
        """ Train the model using a dataframe object as an input
        """
        cut_dataframe = self.cut_dataframe_at_midnight(train_dataframe)
        missing_labels = np.array(cut_dataframe['missing'], dtype='int')
        if all(missing_labels == 0):
            training_data = np.array(cut_dataframe['reportedcount1'])
            self.train(training_data, epochs, batchsize, neurons)
        else:
            print("ERROR: training dataframe cannot contain missing data. Shutting down.")
            exit()

    def test_from_dataframe(self, test_dataframe):
        """ Train the model using a dataframe object as an input
        """
        cut_dataframe = self.cut_dataframe_at_midnight(test_dataframe)
        test_data = np.array(cut_dataframe['reportedcount1'])
        test, pred = self.test(test_data)
        return test, pred

    def cut_times_at_midnight(self, times):
        """ Function used to get the index of the first 00:00 in an array of times
        """
        T_datetime = [datetime.fromtimestamp(t) for t in times]
        is_00 = np.array([t.hour == 0 for t in T_datetime])
        start = np.where(is_00 == True)[0][0]
        return start

    def cut_dataframe_at_midnight(self, dataframe):
        """ Function used to cut at the first 00:00 of the dataframe object
        """
        times = dataframe['startdt']
        start_index = self.cut_times_at_midnight(times)
        cut_dataframe = dataframe[start_index:]
        return cut_dataframe

    def normalize(self, data):
        """ Used to perpare the data for the neural network.
        The average daily profile (typical behaviour of the 5-minute counts during a day)
        is obtained from the data. 
        We then take the log of the raw data and the log of the repeated daily profile, 
        get their difference, and then transform the result so that is has mean = 0 and 
        standard deviation = 1.
        """

        if self.mu is None:
            # Get the weekly profile and tile the dataset
            days_in_dataset = len(data)//288
            daily_profile = np.reshape(
                data[:days_in_dataset*288], (len(data)//288, 288))
            daily_profile = np.mean(daily_profile, axis=0)
            self.daily_profile_single = daily_profile[:288]
            daily_profile = np.tile(daily_profile, len(data)//288 + 1)
            daily_profile = daily_profile[:len(data)]

            # Normalize by taking the logs of the counts and the weekly profile, and then subtracting them
            # +1 is here so that zeros do not break everything
            log_daily_profile = np.log(daily_profile+1)
            # +1 is here so that zeros do not break everything
            X_log = np.log(data+1)
            X_notrend_log = X_log - log_daily_profile
            self.mu = np.mean(X_notrend_log)
            self.std = np.std(X_notrend_log, ddof=1)

        else:
            daily_profile = np.tile(
                self.daily_profile_single, len(data)//288 + 1)
            daily_profile = daily_profile[:len(data)]
            # +1 is here so that zeros do not break everything
            X_notrend_log = np.log(data + 1) - np.log(daily_profile + 1)

        X_notrend_norm_log = (X_notrend_log - self.mu)/self.std
        return X_notrend_norm_log

    def denormalize(self, data):
        """ Used to reconstruct the original data from the output of the neural network.
        Does the inverse operations of normalize
        """
        daily_profile_tile = np.tile(self.daily_profile_single, len(
            data)//288 + 1)  # Tile for a week more
        # And then cut at the data length
        daily_profile_tile = daily_profile_tile[:len(data)]
        # This is needed here otherwise the following operations get messy with dimensions
        data = data[:, 0, 0]
        X_notrend_log = data*self.std + self.mu
        log_daily_profile = np.log(daily_profile_tile+1)
        X_log = X_notrend_log + log_daily_profile
        X = np.exp(X_log) - 1
        return X

    def impute(self, data_to_impute, missing_labels):
        """ Look for data where the missing label is present an impute them based on the num5min 5-minute batches of previous data.
        While imputing, save the imputed values and replace missing data.
        """
        self.impute_data = data_to_impute
        self.missing_labels = missing_labels
        self.normalized_impute_data = self.normalize(self.impute_data)

        self.X_imputed_norm, _ = self.reshape_data(self.normalized_impute_data)
        self.Y_imputed_norm = np.reshape(
            self.normalized_impute_data, (len(self.normalized_impute_data), 1, 1))
        for i in np.where(self.missing_labels == 1)[0]:
            try:
                pred = self.model.predict(np.reshape(
                    self.X_imputed_norm[i-self.num5mins, :, 0], (1, self.num5mins, 1)))
                self.Y_imputed_norm[i] = pred
                for t in range(self.num5mins):
                    if(i+1-self.num5mins+t < len(self.X_imputed_norm)):
                        self.X_imputed_norm[i+1-self.num5mins +
                                            t, self.num5mins-1-t, 0] = pred

            except:
                print(
                    "WARNING: Cannot impute data that has no counts for the previous %i 5-minute periods." % (self.num5mins))
                pass

        self.Y_imputed = self.denormalize(self.Y_imputed_norm)
        return self.impute_data, self.Y_imputed

    def impute_from_dataframe(self, dataframe):
        """ Impute from a dataframe object which has the "missing" label
        """
        cut_dataframe = self.cut_dataframe_at_midnight(dataframe)
        missing_labels = np.array(cut_dataframe['missing'], dtype='int')
        data_to_impute = np.array(cut_dataframe['reportedcount1'])
        test, pred = self.impute(data_to_impute, missing_labels)
        return test, pred

    def save(self, location="../models/"):
        """ Save model and its normalization parameters to json,
        and model's weights to h5 files at location
        """
        # serialize model to JSON
        model_json = self.model.to_json()
        with open(location + "5min_imputation_model.json", "w") as json_file:
            json_file.write(model_json)
        # serialize weights to HDF5
        self.model.save_weights(location + "5min_imputation_model_weights.h5")

        parameters = {'mu': self.mu, 'std': self.std,
                      'daily_profile': list(self.daily_profile_single)}
        with open(location + "5min_imputation_normalization_parameters.json", "w") as fp:
            json.dump(parameters, fp, indent=4)

        print("Saved model to disk")

    def load(self, location="../models/"):
        """ Load model and its normalization parameters from json,
        and model's weights from h5 files at location
        """
        # load json and create model
        json_file = open(location + '5min_imputation_model.json', 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        loaded_model = model_from_json(loaded_model_json)
        # load weights into new model
        loaded_model.load_weights(
            location + "5min_imputation_model_weights.h5")
        self.model = loaded_model

        with open(location + '5min_imputation_normalization_parameters.json') as json_file:
            parameters = json.load(json_file)
        self.mu, self.std = parameters['mu'], parameters['std']
        self.daily_profile_single = np.array(parameters['daily_profile'])
        print("Loaded model from disk")

    def plot_imputation(self, fig_name=None):
        plt.figure(figsize=(20, 10))
        plt.plot(self.impute_data, label='Observed counts')
        plt.plot(self.Y_imputed, label='Imputed counts')

        plt.plot(np.arange(len(self.Y_imputed))[np.where(self.missing_labels == 1)[0]],
                 np.zeros(len(self.Y_imputed))[np.where(self.missing_labels == 1)[0]], 'r.', label='Missing data')
        plt.xlabel('5 min batch')
        plt.ylabel('5 Minute Counts')
        plt.legend()
        if fig_name is not None:
            plt.savefig(fig_name + ".png", dpi=80)
            plt.close()
        else:
            plt.show()


def get_dataframe(folder='../data/5m_agg_counts_2016.csv', locid=2):
    """ Obtain and prepare real data from a legacy database.
    The format of the data is such that it should be almost identical to the one used in the pipeline.
    """
    real_data_5m = pd.read_csv(folder, sep=',', names=[
                               'areaid', 'devid', 'time', 'reportedcount1'], skiprows=[0])
    loc_id = 2
    df_5m = real_data_5m.loc[np.where(real_data_5m['areaid'] == loc_id)[0]]

    start_epochs = np.array(
        [timegm(time.strptime(t, "%Y-%m-%d %H:%M:%S.%f")) for t in df_5m['time']])
    end_epochs = start_epochs + 299
    df_5m['reportedcount2'] = np.zeros(len(df_5m['reportedcount1']))
    df_5m['startdt'] = start_epochs
    df_5m['enddt'] = end_epochs
    df_5m['level'] = "5m"
    df_5m['missing'] = 0
    df_5m['quality'] = 1
    df_5m = df_5m.drop('time', 1)
    df_5m = df_5m.drop('devid', 1)
    return df_5m


def test_accuracy(dataframe, train_days=30, test_days=20, fig_name=None):
    """ Function used to predict imputation accuracy on gaps of 5 minutes, hours, and days.
        The model is trained and then tested on never-seen data which contains missing values
        which the model has to guess. Error is calculated as the % difference between the guess
        and the actual values of the observation at that time.
    """
    a = MinutePredictor(36)
    df_train = dataframe[:288*train_days]
    df_test = dataframe[288*train_days:288*(train_days+test_days)]
    a.train_from_dataframe(df_train)

    # Test on single hours missing, chosen at random
    missing_5mins = np.zeros(len(df_test))
    index_5mins = np.random.choice(
        np.arange(36, len(df_test)), size=36*test_days, replace=False)
    missing_5mins[index_5mins] = 1
    df_test_missing5mins = deepcopy(df_test)
    df_test_missing5mins['missing'] = missing_5mins
    y, t = a.impute_from_dataframe(df_test_missing5mins)

    mean_5min_perc_err_5min = np.mean(np.abs(
        100*(1+t[np.where(a.missing_labels == 1)[0]])/(1+y[np.where(a.missing_labels == 1)[0]]) - 100))

    print("Imputing 5 minute gaps:")
    print("Mean 5 mins  %% error on inputed data: %.2f %%" %
          (mean_5min_perc_err_5min))
    if fig_name is not None:
        a.plot_imputation(fig_name + "5min")
    else:
        a.plot_imputation()

    # Test on single days misisng, chosen at random
    missing_hours = np.zeros(len(df_test))
    index_hours = np.random.choice(
        np.arange(1, len(df_test)//12), size=3*test_days, replace=False)
    for i in index_hours:
        missing_hours[12*i:12*i+12] = 1
    df_test_missinghours = deepcopy(df_test)
    df_test_missinghours['missing'] = missing_hours
    y, t = a.impute_from_dataframe(df_test_missinghours)

    mean_5min_perc_err_hours = np.mean(np.abs(
        100*(1+t[np.where(a.missing_labels == 1)[0]])/(1+y[np.where(a.missing_labels == 1)[0]]) - 100))

    mean_hourly_perc_err_hours = 0
    for i in index_hours:
        mean_hourly_perc_err_hours += abs(100*(1+np.sum(t[12*i:12*i+12]))/(
            1 + np.sum(y[12*i:12*i+12])) - 100) / len(index_hours)

    print("Imputing hourly gaps:")
    print("Mean 5 mins  %% error on inputed data: %.2f %%" %
          (mean_5min_perc_err_hours))
    print("Mean hourly  %% error on inputed data: %.2f %%" %
          (mean_hourly_perc_err_hours))

    if fig_name is not None:
        a.plot_imputation(fig_name + "hourly")
    else:
        a.plot_imputation()

    # Test on single days misisng, chosen at random
    missing_days = np.zeros(len(df_test))
    index_days = np.random.choice(
        np.arange(1, test_days), size=test_days//10 + 1, replace=False)
    for i in index_days:
        missing_days[288*i:288*i+288] = 1
    df_test_missingdays = deepcopy(df_test)
    df_test_missingdays['missing'] = missing_days
    y, t = a.impute_from_dataframe(df_test_missingdays)

    mean_5min_perc_err_days = np.mean(np.abs(
        100*(1+t[np.where(a.missing_labels == 1)[0]])/(1+y[np.where(a.missing_labels == 1)[0]]) - 100))

    mean_hourly_perc_err_days = 0
    for i in index_days:
        for j in np.arange(24):
            mean_hourly_perc_err_days += abs(100*(1+np.sum(t[288*i+12*j:288*i+12*j+12]))/(
                1+np.sum(y[288*i+12*j:288*i+12*j+12])) - 100) / len(index_days)/24

    mean_daily_perc_err_week = 0
    for i in index_days:
        mean_daily_perc_err_week += abs(100*(1+np.sum(t[288*i:288*i+288]))/(
            1+np.sum(y[288*i:288*i+288])) - 100) / len(index_days)

    print("Imputing daily gaps:")
    print("Mean 5 mins %% error on inputed data: %.2f %%" %
          (mean_5min_perc_err_days))
    print("Mean hourly %% error on inputed data: %.2f %%" %
          (mean_hourly_perc_err_days))
    print("Mean daily  %% error on inputed data: %.2f %%" %
          (mean_daily_perc_err_week))

    if fig_name is not None:
        a.plot_imputation(fig_name + "daily")
    else:
        a.plot_imputation()


@click.command()
# Location ID inside the dataframe
@click.option("--location", '-l', type=int, default=2)
# How many days to use in training, these are always the first days of the ones that are used
@click.option("--training_days", "-ntr", type=int, default=6*7)
# How many days to use in testing, these follow immediately the training days
@click.option("--test_days", "-ntst", type=int, default=52*7)
# Location where to save the model to, and load the model from
@click.option("--model_location", "-ml", type=str, default=None)
def main(location, training_days, test_days, model_location):

    df = get_dataframe(folder='../data/5m_agg_counts_2016.csv', locid=location)
    test_accuracy(df, training_days, test_days,
                  fig_name="../plots/5minute_imputation_error_plot")


if __name__ == "__main__":
    main()